// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.7.6;

// The SeedService is designed to be a static entrypoint into the system. 
// The User should be able to connect to THORChain with just this contract address and a Web3 provider.

// The SeedService allows any Asgard to report a list of IPs as a string. 
// The SeedService expects multiple Asgards, so is append-only.

// A user can query to get "last seed" or "recent seed" or seeds from a specific Asgard. 
// Users should not trust any Asgard, so should query multiple times. 
// Paranoid users can check the balance of the Asgard address to see the presence of Asgard funds. 

contract SeedService {

    // Asgard checks
    mapping(address => bool) public isAsgard;

    // Reported seeds for each asgard
    mapping(address => string) public asgardSeeds;
    address[] public historicalAsgards;

    // Emitted when Asgard changes
    event NewAsgard(address indexed oldAsgard, address[] newAsgards);

    // Only Asgard can execute
    modifier onlyAsgard() {
        require(isAsgard[msg.sender], "Must be Asgard");
        _;
    }

    constructor() {
        isAsgard[msg.sender] = true; // Deployer is first Asgard
    }

    // Only an Asgard can add other Asgards (in an array, can be only 1)
    function changeAsgard(address[] memory newAsgards) public onlyAsgard {
        isAsgard[msg.sender] = false;
        for(uint i = 0; i < newAsgards.length; i++){
            address newAsgard = newAsgards[i];
            isAsgard[newAsgard] = true;
            historicalAsgards.push(newAsgard);
        }
        emit NewAsgard(msg.sender, newAsgards);
    }

    // Asgard sets a seed list
    function setSeed(string memory seed) public onlyAsgard {
        asgardSeeds[msg.sender] = seed;
    }

    //############################## SEED SERVICE ##############################

    // Return list of IPs reported by an Asgard
    function getSeedReportedByAsgard(address asgard) public view returns(string memory) {
        return asgardSeeds[asgard];
    }

    // Return the last list of IPs
    function getLastSeed() public view returns(string memory) {
        return asgardSeeds[getLastAsgard()];
    }

    // Return the list of IPs at an index (0:genesis)
    function getSeedAtIndex(uint index) public view returns(string memory) {
        return asgardSeeds[getAsgardAtIndex(index)];
    }

    // Return the list of IPs ordered by recency (0:the most recent)
    function getRecentSeed(uint recency) public view returns(string memory) {
        return asgardSeeds[getRecentAsgard(recency)];
    }


    //############################## HELPERS ##############################
    // Count
    function asgardCount() public view returns(uint){
        return historicalAsgards.length;
    }

    // Return the last Asgard
    function getLastAsgard() public view returns(address) {
        return historicalAsgards[asgardCount()-1];
    }

    // Return the Asgard at an index (0:genesis)
    function getAsgardAtIndex(uint index) public view returns(address) {
        return historicalAsgards[index];
    }

    // Return the Asgard ordered by recency (0:the most recent)
    function getRecentAsgard(uint recency) public view returns(address) {
        return historicalAsgards[asgardCount()-1-recency];
    }

}
