/*
* This test deploys the Seed Service, then adds some IPs
* It then adds more Asgards, and queries for IPs
*/

const SeedService = artifacts.require("SeedService");
const BigNumber = require('bignumber.js')
const truffleAssert = require('truffle-assertions')

function BN2Str(BN) { return ((new BigNumber(BN)).toFixed()) }
function getBN(BN) { return (new BigNumber(BN)) }

var USER1;
var ASGARD1, ASGARD2, ASGARD3;


describe("SeedService contract", function () {
  let accounts;

  before(async function () {
    accounts = await web3.eth.getAccounts();
    SEEDSERVICE = await SeedService.new();
    USER1 = accounts[0]
    ASGARD1 = accounts[3]
    ASGARD2 = accounts[4]
    ASGARD3 = accounts[5]
  });


  describe("Construction", function () {

    it("Should deploy and set Asgard", async function () {
      await SEEDSERVICE.changeAsgard([ASGARD1], { from: USER1 })
      expect(await SEEDSERVICE.isAsgard(USER1)).to.equal(false);
      expect(await SEEDSERVICE.isAsgard(ASGARD1)).to.equal(true);
    });

    it("Asgard should set first seed", async function () {

      await SEEDSERVICE.setSeed('1.2.3.4;10.11.12.13', {from:ASGARD1});
      expect(await SEEDSERVICE.getSeedReportedByAsgard(ASGARD1)).to.equal('1.2.3.4;10.11.12.13');
      expect(await SEEDSERVICE.getLastSeed()).to.equal('1.2.3.4;10.11.12.13');
      expect(await SEEDSERVICE.getSeedAtIndex('0')).to.equal('1.2.3.4;10.11.12.13');
      expect(await SEEDSERVICE.getRecentSeed('0')).to.equal('1.2.3.4;10.11.12.13');

    });

  });

  describe("New Asgard", function () {

    it("Old Asgard sets newAsgards", async function () {
      await SEEDSERVICE.changeAsgard([ASGARD2, ASGARD3], { from: ASGARD1 })
      expect(await SEEDSERVICE.isAsgard(ASGARD1)).to.equal(false);
      expect(await SEEDSERVICE.isAsgard(ASGARD2)).to.equal(true);
      expect(await SEEDSERVICE.isAsgard(ASGARD3)).to.equal(true);
    });

    it("New Asgards should set seeds", async function () {

      await SEEDSERVICE.setSeed('10.11.12.13;100.110.130.140', {from:ASGARD2});
      await SEEDSERVICE.setSeed('100.110.130.140;200.210.220.230', {from:ASGARD3});
      expect(await SEEDSERVICE.getSeedReportedByAsgard(ASGARD2)).to.equal('10.11.12.13;100.110.130.140');
      expect(await SEEDSERVICE.getLastSeed()).to.equal('100.110.130.140;200.210.220.230');
      expect(await SEEDSERVICE.getSeedAtIndex('1')).to.equal('10.11.12.13;100.110.130.140');
      expect(await SEEDSERVICE.getRecentSeed('1')).to.equal('10.11.12.13;100.110.130.140');

    });

  });
  

});
